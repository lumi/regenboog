#![deny(missing_docs)]
#![deny(unsafe_code)]

//! A crate for handling colors.

/// A type which represents a color.
///
/// This color is defined by four components:
///  - red
///  - green
///  - blue
///  - alpha
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct RgbaU8 {
    /// The red component
    pub red: u8,
    /// The green component
    pub green: u8,
    /// The blue component
    pub blue: u8,
    /// The alpha component
    pub alpha: u8,
}

impl RgbaU8 {
    /// Pure white
    pub const WHITE: RgbaU8 = RgbaU8::rgb(255, 255, 255);

    /// Pure black
    pub const BLACK: RgbaU8 = RgbaU8::rgb(0, 0, 0);

    /// Red
    pub const RED: RgbaU8 = RgbaU8::rgb(255, 0, 0);

    /// Green
    pub const GREEN: RgbaU8 = RgbaU8::rgb(0, 255, 0);

    /// Blue
    pub const BLUE: RgbaU8 = RgbaU8::rgb(0, 0, 255);

    /// Construct a color from its red, green and blue components.
    ///
    /// The alpha value is set to 255.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     regenboog::RgbaU8,
    /// # };
    /// let swag_color = RgbaU8::rgb(160, 32, 240);
    /// assert_eq!(swag_color.red  , 160);
    /// assert_eq!(swag_color.green, 32 );
    /// assert_eq!(swag_color.blue , 240);
    /// assert_eq!(swag_color.alpha, 255);
    /// ```
    pub const fn rgb(red: u8, green: u8, blue: u8) -> RgbaU8 {
        RgbaU8 {
            red,
            green,
            blue,
            alpha: 255,
        }
    }

    /// Construct a color from its red, green, blue and alpha components.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     regenboog::RgbaU8,
    /// # };
    /// let swag_color_transparent = RgbaU8::rgba(160, 32, 240, 200);
    /// assert_eq!(swag_color_transparent.red  , 160);
    /// assert_eq!(swag_color_transparent.green, 32 );
    /// assert_eq!(swag_color_transparent.blue , 240);
    /// assert_eq!(swag_color_transparent.alpha, 200);
    /// ```
    pub const fn rgba(red: u8, green: u8, blue: u8, alpha: u8) -> RgbaU8 {
        RgbaU8 {
            red,
            green,
            blue,
            alpha,
        }
    }

    /// Get a new color with the specified `red` component.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     regenboog::RgbaU8,
    /// # };
    /// let yellow = RgbaU8::GREEN.with_red(255);
    /// assert_eq!(yellow, RgbaU8::rgb(255, 255, 0));
    /// ```
    pub const fn with_red(mut self, red: u8) -> RgbaU8 {
        self.red = red;
        self
    }

    /// Get a new color with the specified `green` component.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     regenboog::RgbaU8,
    /// # };
    /// let cyan = RgbaU8::BLUE.with_green(255);
    /// assert_eq!(cyan, RgbaU8::rgb(0, 255, 255));
    /// ```
    pub const fn with_green(mut self, green: u8) -> RgbaU8 {
        self.green = green;
        self
    }

    /// Get a new color with the specified `blue` component.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     regenboog::RgbaU8,
    /// # };
    /// let magenta = RgbaU8::BLUE.with_red(255);
    /// assert_eq!(magenta, RgbaU8::rgb(255, 0, 255));
    /// ```
    pub const fn with_blue(mut self, blue: u8) -> RgbaU8 {
        self.blue = blue;
        self
    }

    /// Get a new color with the specified `alpha` component.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     regenboog::RgbaU8,
    /// # };
    /// let transparent_blue = RgbaU8::BLUE.with_alpha(100);
    /// assert_eq!(transparent_blue, RgbaU8::rgba(0, 0, 255, 100));
    /// ```
    pub const fn with_alpha(mut self, alpha: u8) -> RgbaU8 {
        self.alpha = alpha;
        self
    }
}

fn to_f32(u: u8) -> f32 {
    (u as f32) / 255.
}

fn from_f32(f: f32) -> u8 {
    (f * 255.) as u8
}

impl From<RgbaU8> for [u8; 3] {
    fn from(color: RgbaU8) -> [u8; 3] {
        [color.red, color.green, color.blue]
    }
}

impl From<RgbaU8> for [u8; 4] {
    fn from(color: RgbaU8) -> [u8; 4] {
        [color.red, color.green, color.blue, color.alpha]
    }
}

impl From<[u8; 3]> for RgbaU8 {
    fn from(data: [u8; 3]) -> RgbaU8 {
        RgbaU8 {
            red: data[0],
            green: data[1],
            blue: data[2],
            alpha: 255,
        }
    }
}

impl From<[u8; 4]> for RgbaU8 {
    fn from(data: [u8; 4]) -> RgbaU8 {
        RgbaU8 {
            red: data[0],
            green: data[1],
            blue: data[2],
            alpha: data[3],
        }
    }
}

impl From<RgbaU8> for [f32; 3] {
    fn from(color: RgbaU8) -> [f32; 3] {
        [to_f32(color.red), to_f32(color.green), to_f32(color.blue)]
    }
}

impl From<RgbaU8> for [f32; 4] {
    fn from(color: RgbaU8) -> [f32; 4] {
        [to_f32(color.red), to_f32(color.green), to_f32(color.blue), to_f32(color.alpha)]
    }
}

impl From<[f32; 3]> for RgbaU8 {
    fn from(data: [f32; 3]) -> RgbaU8 {
        RgbaU8 {
            red: from_f32(data[0]),
            green: from_f32(data[1]),
            blue: from_f32(data[2]),
            alpha: 255,
        }
    }
}

impl From<[f32; 4]> for RgbaU8 {
    fn from(data: [f32; 4]) -> RgbaU8 {
        RgbaU8 {
            red: from_f32(data[0]),
            green: from_f32(data[1]),
            blue: from_f32(data[2]),
            alpha: from_f32(data[3]),
        }
    }
}
